import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  Image,
  TouchableOpacity,
  Button,
  FlatList,
} from 'react-native';

import ConexionFetch from './app/components/conexionFetch/ConexionFetch';

import Message from './app/components/message/Message';

import Body from './app/components/body/Body';

import OurFlatList from './app/components/ourFlatList/OurFlatList';


import {NavigationContainer} from '@react-navigation/native';

import {createStackNavigator} from '@react-navigation/stack';

function HomeScreen({navigation}) {
  return (
    <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
      <Text>Home Screen</Text>
      <Button
        title="Go to Details"
        onPress={() => navigation.navigate('Details')}
      />
    </View>
  );
}

function Lista({navigation}) {
  return (
    <OurFlatList navigation={navigation}/>
  );
}

function DetailsScreen({route, navigation}) {

  const {itemTitle} = route.params;
  const {itemID} = route.params;
  const {itemDesc} = route.params;
  const {itemPic} = route.params;

  return (
    <View style={{backgroundColor: '#212121', flex: 1, alignItems: 'center', justifyContent: 'center'}}>
      
        <Text style={{color: 'white' , fontSize: 40, marginBottom: 20}}>{itemTitle}</Text>
        <Image style={{aspectRatio: 2/3 , width:250 , marginBottom: 20} } source={{uri: itemPic}} />
        <Text style={{color: 'white' , fontSize: 24, marginBottom: 20}}>Descripcion: {itemDesc}</Text>

      <TouchableOpacity onPress = {() => navigation.popToTop()}>
        <View style = {{padding:10, backgroundColor: '#2196f3', alignItems: 'center', justifyContent: 'center', borderRadius: 4}}>
          <Text style = {{fontSize: 20, color: 'white'}}>VOLVER</Text>
        </View>
      </TouchableOpacity>
    </View>
  );
}

const Stack = createStackNavigator();

const provincias = [
  {
    id: 1,
    name: 'Arequipa',
  },
  {
    id: 2,
    name: 'Puno',
  },
  {
    id: 3,
    name: 'Cuzco',
  },
];

export default class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      textValue: 0,
      count: 0,
      items: [],
      error: null,
    };
  }

  componentDidMount() {
    console.warn('Estamos dentro componentDidMount', this.state.textValue);
  }

  shouldComponentUpdate() {
    console.warn('Estamos dentro shouldComponentUpdate', this.state.error);
  }

  changeTextInput = text => {
    this.setState({textValue: text});
  };

  onPress = () => {
    this.setState({
      count: this.state.count + 1,
    });
  };

  render() {
    return (
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen name="List" component={Lista}/>
          <Stack.Screen name="Details" component={DetailsScreen} />
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}

const styles = StyleSheet.create({
  text: {
    alignItems: 'center',
    padding: 10,
  },

  button: {
    top: 10,
    fontSize: 50,
    alignItems: 'center',
    backgroundColor: '#DDDDDD',
    padding: 10,
  },
  countContainer: {
    alignItems: 'center',
    padding: 10,
  },
  countText: {
    color: '#FF00FF',
  },

  container: {
    flex: 1,
    marginTop: 20,
  },
  item: {
    backgroundColor: 'orange',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  title: {
    fontSize: 32,
  },
});
